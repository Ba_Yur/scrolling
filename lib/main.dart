import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  static List _listOfData = [
    {'name': 'Bob', 'age': '23'},
    {'name': 'Genry', 'age': '11'},
    {'name': 'Vityok', 'age': '23'},
    {'name': 'Nataha', 'age': '543'},
    {'name': 'Gary', 'age': '56'},
    {'name': 'Устал', 'age': '67'},
    {'name': 'Придумывать имена)', 'age': '87'},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Scrolling'),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return Container(
            height: 200,
            color: Colors.redAccent,
            margin: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('Name: ${_listOfData[index]['name']}'),
                Text('Age: ${_listOfData[index]['age']}')
              ],
            ),
          );
        },
        itemCount: _listOfData.length,
      ),
    );
  }
}
